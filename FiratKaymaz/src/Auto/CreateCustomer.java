package Auto;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateCustomer {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		//CREATE CUSTOMER
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		WebDriver driver = new InternetExplorerDriver();
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");
		//SECURITY PAGE
		//LineActivation.SecurityPage(driver);
		//LOGIN PAGE
		LineActivation.LoginPage(driver);
		CustomerCreation(driver);
		CustomerCreationv2(driver);
		CustomerCreationv3(driver);
		CustomerCreationv4(driver);
		CustomerCreation5(driver);
		CustomerCreation6(driver);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		System.out.println(driver.findElement(By.tagName("h2")).getText());

	}
	
	public static void CustomerCreation (WebDriver driver){
		
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='M��teri Olu�turma']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='Submit']")).click();	
	}
	
	public static void CustomerCreationv2 (WebDriver driver) throws InterruptedException{
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='name']")).sendKeys("KEM");
		driver.findElement(By.xpath("//input[@name='lastname']")).sendKeys("KEM");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='day']")));
		selectObject.selectByValue("10");
		Thread.sleep(1000);
		Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@name='month']")));
		selectObject2.selectByValue("10");
		Thread.sleep(1000);
		Select selectObject3 = new Select (driver.findElement(By.xpath("//select[@name='year']")));
		selectObject3.selectByValue("1950");
		Thread.sleep(1000);
		String mainWindow = driver.getWindowHandle();
		driver.findElement(By.xpath("//input[@name='iddistrict']")).sendKeys("ADANA");
		driver.findElement(By.xpath("//input[@name='fathername']")).sendKeys("BABA");
		driver.findElement(By.xpath("//input[@name='Submit']")).click();
		LineActivation.Windows(driver, mainWindow);
		LineActivation.SecuritPageCheck(driver);
		driver.switchTo().window(mainWindow);
	}
	
	public static void CustomerCreationv3 (WebDriver driver) throws InterruptedException {
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='tcidno']")).sendKeys("74765785526");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='d_day']")));
		selectObject.selectByValue("10");
		Thread.sleep(1000);
		Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@name='d_month']")));
		selectObject2.selectByValue("10");
		Thread.sleep(1000);
		Select selectObject3 = new Select (driver.findElement(By.xpath("//select[@name='d_year']")));
		selectObject3.selectByValue("1950");
		driver.findElement(By.xpath("//input[@name='ver_birthplace']")).sendKeys("ADANA");
		driver.findElement(By.xpath("//input[@name='ver_mothername']")).sendKeys("ANNE");
		Select selectObject4 = new Select (driver.findElement(By.xpath("//select[@name='ver_gender']")));
		selectObject4.selectByValue("E");
		driver.findElement(By.xpath("//input[@name='mernissorgu']")).click();
		Thread.sleep(1000);
		driver.switchTo().alert().accept();
		
	}
	
	public static void CustomerCreationv4 (WebDriver driver) throws InterruptedException{
		
		LineActivation.switchwindow2(driver, "cx", "frame1");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='idtype']")));
		selectObject.selectByValue("N");
		Thread.sleep(1000);
		String kimlikno = driver.findElement(By.xpath("//input[@name='tcidno']")).getAttribute("value").toString();
		driver.findElement(By.xpath("//input[@name='idno']")).sendKeys(kimlikno);
		driver.findElement(By.xpath("//input[@name='mothermaidenname']")).sendKeys("ANNE");
		Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@name='education']")));
		selectObject2.selectByValue("�niversite");
		Thread.sleep(1000);
		Select selectObject3 = new Select (driver.findElement(By.xpath("//select[@name='workingstatus']")));
		selectObject3.selectByValue("ISDURUMU1");
		Thread.sleep(1000);
		Select selectObject4 = new Select (driver.findElement(By.xpath("//select[@name='workingplace']")));
		selectObject4.selectByValue("ISYERITIPI1");
		Thread.sleep(1000);
		Select selectObject5 = new Select (driver.findElement(By.xpath("//select[@name='workingsector']")));
		selectObject5.selectByValue("SEKTOR2");
		Thread.sleep(1000);
		Select selectObject6 = new Select (driver.findElement(By.xpath("//select[@name='job']")));
		selectObject6.selectByValue("MESLEK8");
		Thread.sleep(1000);
		Select selectObject7 = new Select (driver.findElement(By.xpath("//select[@name='house']")));
		selectObject7.selectByValue("EVDURUMU3");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@name='otherGsmNo']")).sendKeys("5342920202");
		driver.findElement(By.xpath("//input[@name='email1']")).sendKeys("otomoto");
		driver.findElement(By.xpath("//input[@name='email2']")).sendKeys("outlook.com");
		WebElement adresgirisi=driver.findElement(By.xpath("//td[@class='passive' and @id='2' and @style='CURSOR: hand']"));
		adresgirisi.click();
		
	}
	
	public static void CustomerCreation5 (WebDriver driver) throws InterruptedException{
		
		LineActivation.switchwindow2(driver, "cx", "frame1");
		driver.findElement(By.xpath("//a[@href='/TelsimGlobal/AppControlCenter?page=ccbcustaddress&action=ekleme']")).click();
		Thread.sleep(2000);
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='idcity']")));
		selectObject.selectByValue("34");
		Thread.sleep(2000);
		Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@name='idtown']")));
		selectObject2.selectByValue("34000034000");
		Thread.sleep(2000);
		Select selectObject3 = new Select (driver.findElement(By.xpath("//select[@name='neighborhoodId']")));
		selectObject3.selectByValue("34000034001");
		Thread.sleep(2000);
		Select selectObject4 = new Select (driver.findElement(By.xpath("//select[@name='boulevardAvenueId']")));
		selectObject4.selectByValue("857705");
		Thread.sleep(2000);
		Select selectObject5 = new Select (driver.findElement(By.xpath("//select[@name='streetId']")));
		selectObject5.selectByValue("852383");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='place']")).sendKeys("mevki");
		Select selectObject6 = new Select (driver.findElement(By.xpath("//select[@name='siteId']")));
		selectObject6.selectByValue("-1");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='site']")).sendKeys("site");
		Select selectObject7 = new Select (driver.findElement(By.xpath("//select[@name='buildingApartmentNo']")));
		selectObject7.selectByValue("-1");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='apartmentnumber']")).sendKeys("PINAR");
		driver.findElement(By.xpath("//input[@name='flatnumber']")).sendKeys("3");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='kaydet']")).click();
		LineActivation.switchwindow2(driver, "cx", "frame1");
		WebElement odeme=driver.findElement(By.xpath("//td[@class='passive' and @id='3' and @style='CURSOR: hand']"));
		odeme.click();
	}
	public static void CustomerCreation6 (WebDriver driver){
		
		LineActivation.switchwindow2(driver, "cx", "frame1");
		Select selectObject7 = new Select (driver.findElement(By.xpath("//select[@name='ptype']")));
		selectObject7.selectByValue("Diger");
		driver.findElement(By.xpath("//input[@name='1']")).sendKeys("odeme");
		driver.findElement(By.xpath("//input[@name='kaydet']")).click();
		driver.findElement(By.xpath("//input[@name='kaydet1']")).click();
	}

}
