package Auto;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;

public class MernisInquiry {

	public static void main(String[] args) throws InterruptedException {
		
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		WebDriver driver = new InternetExplorerDriver();
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");
		// TODO Auto-generated method stub
		Login(driver);
		YeniSorgulama(driver);
		InquiryCustomer(driver);
		SairIslem(driver);
		MernisLink(driver);
		TcKimlikNo(driver);
		VerificationComment(driver);

	}
	public static void Login(WebDriver driver){
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("wiprobaslan");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda123456789**");
		WebElement login=driver.findElement(By.xpath("//input[@src='/TelsimGlobal/Menu/image3.gif']"));
		login.click();
	}
	public static void YeniSorgulama(WebDriver driver){
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='M��teri-Abone Listesi']")).click();
	}
	public static void InquiryCustomer(WebDriver driver){
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='gsmno']")).sendKeys("5343352112");
		driver.findElement(By.xpath("//input[@name='bul']")).sendKeys(Keys.ENTER);
	}
	public static void SairIslem(WebDriver driver) throws InterruptedException{
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@value='Sair i�lem']")).sendKeys(Keys.ENTER);
	}
	public static void MernisLink(WebDriver driver) throws InterruptedException{
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.cssSelector("//a[href*='396']")).sendKeys(Keys.ENTER);
	}
	public static void TcKimlikNo(WebDriver driver) throws InterruptedException{
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='treason']")));
		selectObject.selectByValue("AB.ISTEGI");
		driver.findElement(By.xpath("//input[@name='description']")).sendKeys("mernis");
		driver.findElement(By.xpath("//input[@name='T.C. Kimlik No Sorgulama']")).sendKeys(Keys.ENTER);
	}
	public static void VerificationComment(WebDriver driver) throws InterruptedException{
	Thread.sleep(2000);
	String a = driver.findElement(By.xpath("//b[contains(text(),'Islem Basarili Olmustur.')]")).getText();
	System.out.println(a);
	}
	
	
}
