package Auto;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MnpActivation {

	public static void main(String[] args) throws InterruptedException, ClassNotFoundException, SQLException, AWTException {
		// TODO Auto-generated method stub
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		WebDriver driver = new InternetExplorerDriver();
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");
		LineActivation.LoginPage(driver);
		driver.switchTo().frame("bx");
		driver.findElement(By.xpath("//input[@value='MNP']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='MNP Ba�vuru Ekrani']")).click();
		LineActivation.RoadtoActivation(driver);
		String mainWindow = driver.getWindowHandle();
		LineActivation.Windows(driver, mainWindow);
		LineActivation.SecuritPageCheck(driver);
		driver.switchTo().window(mainWindow);
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@value='Devam']")).click();
		driver.switchTo().alert().accept();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		WebElement aboneislemleri=driver.findElement(By.xpath("//td[@class='passive' and @id='2' and @style='CURSOR: hand']"));
		aboneislemleri.click();
		MNPSimcardSelect(driver);
		LineActivation.PaketSecimi(driver);
		LineActivation.OdemeTipi(driver);
		LineActivation.FaturaDonemi(driver);
		LineActivation.KampanyaGirisi(driver);
		LineActivation.KullaniciGirisi(driver);
		LineActivation.VekilVasi(driver);
		LineActivation.OzellikGiris(driver);
		LineActivation.ServisGiris(driver);
		LineActivation.switchwindow2(driver,"cx","frame1");
		driver.findElement(By.xpath("//input[@id='activation']")).click();
		driver.switchTo().alert().accept();
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		System.out.println(driver.findElement(By.tagName("h2")).getText());
		MNPBasvuru(driver);
		DKT(driver);
	}

   public static void MNPSimcardSelect (WebDriver driver) throws InterruptedException, ClassNotFoundException, SQLException{
	   
	    String mainWindow = driver.getWindowHandle();
		driver.switchTo().defaultContent();
		LineActivation.switchwindow(driver, "cx", "frame1", "frame2");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='brand']")));
		selectObject.selectByValue("MC");
		WebDriverWait wait1 = new WebDriverWait(driver, 15);
		wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[@name='prefix']")));
		Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@name='prefix']")));
		selectObject2.selectByValue("534");
		wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[@name='rsvstatus']")));
		Select selectObject3 = new Select (driver.findElement(By.xpath("//select[@name='rsvstatus']")));
		selectObject3.selectByValue("E");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@name='buttongsmno']")).click();
		Thread.sleep(7000);
		LineActivation.Windows(driver, mainWindow);
		Random rand = new Random();
		int num = rand.nextInt(9000000) + 1000000;
		String number = Integer.toString(num);
		driver.findElement(By.xpath("//input[@name='selgsmno']")).sendKeys(number);
		driver.findElement(By.xpath("//input[@name='find']")).click();
		driver.findElement(By.xpath("//input[@class='button']")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.switchTo().window(mainWindow);
		LineActivation.switchwindow(driver,"cx","frame1","frame1");
		driver.findElement(By.xpath("//input[@name='simcardICCI']")).click();
		LineActivation.Windows(driver,mainWindow);
		String simcardicci = null;
		driver.findElement(By.xpath("//input[@name='icci']")).clear();
		driver.findElement(By.xpath("//input[@name='icci']")).sendKeys(DBConnectionICCI(simcardicci));
		driver.findElement(By.xpath("//input[@value='BUL']")).click();
		driver.findElement(By.xpath("//input[@type='button']")).click();
		driver.findElement(By.xpath("//input[@class='button']")).sendKeys(Keys.ENTER);
		driver.switchTo().window(mainWindow);
		LineActivation.switchwindow(driver,"cx","frame1","frame1"); 
		driver.findElement(By.xpath("//input[@name='handset']")).click(); //Unexpected error
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		LineActivation.Windows(driver,mainWindow);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@name='handsetbrand']")).sendKeys("Alcatel HB 100");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@name='Bul']")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.cssSelector("//a[href*='AL00004']")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.switchTo().window(mainWindow);
		LineActivation.switchwindow(driver,"cx","frame1","frame1");
		driver.findElement(By.xpath("//input[@name='imeino']")).sendKeys("123456789123456");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("oto");
		LineActivation.switchwindow2(driver,"cx","frame1");
		WebElement paketsecimi=driver.findElement(By.xpath("//td[@class='passive' and @id='2' and @style='CURSOR: hand']"));
		paketsecimi.click();
   }
	public static String DBConnectionICCI(String icci) throws ClassNotFoundException, SQLException{
		
		 Class.forName("oracle.jdbc.OracleDriver");
		   Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
		   Statement stmt = conn.createStatement();
		   stmt.executeQuery("SELECT A.ICCI, B.CAMPAIGN_CODE FROM CCB.CCB_IMSI_POOL A INNER JOIN CCB.CCB_ICCI_SIM_POOL B ON A.ICCI = B.ICCI WHERE A.GSM_NO IS NULL AND A.START_DATE IS NULL AND A.IMSI_STATUS_CODE = 'PS' AND A.BRAND_CODE = 'MC' AND B.USAGE_REASON_CODE = 'AK' AND B.LOCK_IND = 'H' AND B.ICCI_STATUS_CODE = 'BA'AND B.CAMPAIGN_CODE = 'MNP000'AND A.ICCI LIKE '89900292%'");
		   ResultSet rs = stmt.getResultSet();
		   if(rs.next()){
			  String s  = rs.getString(1);
			  icci = s;
		   }
		return icci;
	}
	public static void MNPBasvuru (WebDriver driver) throws InterruptedException, AWTException{
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='Aktivasyon']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='customer' and @type='radio' and @value='4644247716']")).click();
		driver.findElement(By.xpath("//input[@value='Devam']")).click();
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='preferDate00']")));
		selectObject.selectByValue("08:00-16:00");	
		String mainWindow = driver.getWindowHandle();
		driver.findElement(By.xpath("//input[@value='Ba�vuru Olu�tur']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@name='send1']")).click();
		LineActivation.Windows(driver, mainWindow);
		System.out.println(driver.findElement(By.tagName("h3")).getText());
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_W);
		robot.keyRelease(KeyEvent.VK_W);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		driver.switchTo().window(mainWindow);
	}
	public static void DKT(WebDriver driver){
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='Basvuru Alma (Port In)']")).click();
		
	}
}
