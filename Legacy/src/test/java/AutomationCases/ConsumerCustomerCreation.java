package AutomationCases;

import java.io.File;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;



public class ConsumerCustomerCreation {
	
	WebDriver driver;
	
	@BeforeTest
	public void SetUp(){
		
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		driver = new InternetExplorerDriver();
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");	
	}

	@Test(priority=1, description="Login functionality")
	public void LogintoCrm(){
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("fkaymaz");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda123456789**");
		WebElement login=driver.findElement(By.xpath("//input[@src='/TelsimGlobal/Menu/image3.gif']"));
		login.click();
	}
	@Test(priority=2, description="Click Musteri Olusturma on menu")
	public void CustomerCreation(){
		
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='M��teri Olu�turma']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='Submit']")).click();	
	}
	
	@Test(priority=3, description="Type Customer Informations")
	public void CustomerCreationv2 () throws InterruptedException{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='name']")).sendKeys("SANTINO");
		driver.findElement(By.xpath("//input[@name='lastname']")).sendKeys("SANTINO");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='day']")));
		selectObject.selectByValue("10");
		Thread.sleep(1000);
		Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@name='month']")));
		selectObject2.selectByValue("10");
		Thread.sleep(1000);
		Select selectObject3 = new Select (driver.findElement(By.xpath("//select[@name='year']")));
		selectObject3.selectByValue("1950");
		Thread.sleep(1000);
		String mainWindow = driver.getWindowHandle();
		driver.findElement(By.xpath("//input[@name='iddistrict']")).sendKeys("ADANA");
		driver.findElement(By.xpath("//input[@name='fathername']")).sendKeys("BABA");
		driver.findElement(By.xpath("//input[@name='Submit']")).click();
		Windows(driver, mainWindow);
		SecurityPageCheck();
		driver.switchTo().window(mainWindow);
	}
	public void Windows (WebDriver driver, String mainWindow) {
		
		mainWindow = driver.getWindowHandle();
		Set<String> totalwindowsIds = driver.getWindowHandles();
		int a=0;
		String[] myset = totalwindowsIds.toArray(new String[totalwindowsIds.size()]);	
			if(myset[a].contains(mainWindow)){
				driver.switchTo().window(myset[a+1]);
			}
			else{
				driver.switchTo().window(myset[a]);
			}
	}
	public void switchwindow (WebDriver driver, String cx, String frame1, String frame2){
		//WebDriver x;
		driver.switchTo().frame("cx");
		driver.switchTo().frame("frame1");
		driver.switchTo().frame("frame1");	
	}
	public void switchwindow2 (WebDriver driver,String cx, String frame1){
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.switchTo().frame("frame1");
	}
	public void SecurityPageCheck (){
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(!driver.getTitle().toString().contains("secure")){
			driver.findElement(By.xpath("//input[@value='Tamam']")).click();
	}else
	{
		driver.findElement(By.xpath("//img[@class='actionIcon']")).click();
		driver.findElement(By.xpath("//a[@id='overridelink']")).click();
		driver.findElement(By.xpath("//input[@value='Tamam']")).click();
		}
		
	}
	@Test(priority=4, description="Continue to type Customer Informations")
	public  void CustomerCreationv3 () throws InterruptedException {
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='tcidno']")).sendKeys("31282746126");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='d_day']")));
		selectObject.selectByValue("10");
		Thread.sleep(1000);
		Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@name='d_month']")));
		selectObject2.selectByValue("10");
		Thread.sleep(1000);
		Select selectObject3 = new Select (driver.findElement(By.xpath("//select[@name='d_year']")));
		selectObject3.selectByValue("1950");
		driver.findElement(By.xpath("//input[@name='ver_birthplace']")).sendKeys("ADANA");
		driver.findElement(By.xpath("//input[@name='ver_mothername']")).sendKeys("MOM");
		Select selectObject4 = new Select (driver.findElement(By.xpath("//select[@name='ver_gender']")));
		selectObject4.selectByValue("E");
		driver.findElement(By.xpath("//input[@name='mernissorgu']")).click();
		Thread.sleep(1000);
		driver.switchTo().alert().accept();		
	}
	
	@Test(priority=5, description="Proceed to type concerned Customer Informations")
	public void CustomerCreationv4 () throws InterruptedException{
		
		switchwindow2(driver, "cx", "frame1");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='idtype']")));
		selectObject.selectByValue("N");
		Thread.sleep(1000);
		String kimlikno = driver.findElement(By.xpath("//input[@name='tcidno']")).getAttribute("value").toString();
		driver.findElement(By.xpath("//input[@name='idno']")).sendKeys(kimlikno);
		driver.findElement(By.xpath("//input[@name='mothermaidenname']")).sendKeys("ANNE");
		Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@name='education']")));
		selectObject2.selectByValue("�niversite");
		Thread.sleep(1000);
		Select selectObject3 = new Select (driver.findElement(By.xpath("//select[@name='workingstatus']")));
		selectObject3.selectByValue("ISDURUMU1");
		Thread.sleep(1000);
		Select selectObject4 = new Select (driver.findElement(By.xpath("//select[@name='workingplace']")));
		selectObject4.selectByValue("ISYERITIPI1");
		Thread.sleep(1000);
		Select selectObject5 = new Select (driver.findElement(By.xpath("//select[@name='workingsector']")));
		selectObject5.selectByValue("SEKTOR2");
		Thread.sleep(1000);
		Select selectObject6 = new Select (driver.findElement(By.xpath("//select[@name='job']")));
		selectObject6.selectByValue("MESLEK8");
		Thread.sleep(1000);
		Select selectObject7 = new Select (driver.findElement(By.xpath("//select[@name='house']")));
		selectObject7.selectByValue("EVDURUMU3");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@name='otherGsmNo']")).sendKeys("5342920202");
		driver.findElement(By.xpath("//input[@name='email1']")).sendKeys("otomoto");
		driver.findElement(By.xpath("//input[@name='email2']")).sendKeys("outlook.com");
		WebElement adresgirisi=driver.findElement(By.xpath("//td[@class='passive' and @id='2' and @style='CURSOR: hand']"));
		adresgirisi.click();
		
	}
	
	@Test(priority=6, description="type customer's address informations")
	public void CustomerCreation5 () throws InterruptedException{
		
		switchwindow2(driver, "cx", "frame1");
		driver.findElement(By.xpath("//a[@href='/TelsimGlobal/AppControlCenter?page=ccbcustaddress&action=ekleme']")).click();
		Thread.sleep(2000);
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='idcity']")));
		selectObject.selectByValue("34");
		Thread.sleep(2000);
		Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@name='idtown']")));
		selectObject2.selectByValue("34000034000");
		Thread.sleep(2000);
		Select selectObject3 = new Select (driver.findElement(By.xpath("//select[@name='neighborhoodId']")));
		selectObject3.selectByValue("34000034001");
		Thread.sleep(2000);
		Select selectObject4 = new Select (driver.findElement(By.xpath("//select[@name='boulevardAvenueId']")));
		selectObject4.selectByValue("857705");
		Thread.sleep(2000);
		Select selectObject5 = new Select (driver.findElement(By.xpath("//select[@name='streetId']")));
		selectObject5.selectByValue("852383");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='place']")).sendKeys("mevki");
		Select selectObject6 = new Select (driver.findElement(By.xpath("//select[@name='siteId']")));
		selectObject6.selectByValue("-1");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='site']")).sendKeys("site");
		Select selectObject7 = new Select (driver.findElement(By.xpath("//select[@name='buildingApartmentNo']")));
		selectObject7.selectByValue("-1");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='apartmentnumber']")).sendKeys("PINAR");
		driver.findElement(By.xpath("//input[@name='flatnumber']")).sendKeys("3");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='kaydet']")).click();
		switchwindow2(driver, "cx", "frame1");
		WebElement odeme=driver.findElement(By.xpath("//td[@class='passive' and @id='3' and @style='CURSOR: hand']"));
		odeme.click();
	}
	
	@Test(priority=7, description="type customer's billing informations")
	public  void CustomerCreation6 (){
		switchwindow2(driver, "cx", "frame1");
		Select selectObject7 = new Select (driver.findElement(By.xpath("//select[@name='ptype']")));
		selectObject7.selectByValue("Diger");
		driver.findElement(By.xpath("//input[@name='1']")).sendKeys("odeme");
		driver.findElement(By.xpath("//input[@name='kaydet']")).click();
		driver.findElement(By.xpath("//input[@name='kaydet1']")).click();
	}
	
	@Test(priority=8, description="get customer's number")
	public  void CustomerCreation7 (){
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		System.out.println(driver.findElement(By.tagName("h2")).getText());
	}

}
