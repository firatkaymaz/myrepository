package AutomationCases;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ConsumerLineActivation {
	
	WebDriver driver;
	
	
	
	@BeforeTest
	public void SetUp(){
		
		WebDriverManager.iedriver().setup();
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capabilities.setCapability("requireWindowFocus", true);
		//File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		//System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		driver = new InternetExplorerDriver(capabilities);
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");	
	}
	@Test(priority=1, description="Login functionality")
	public void LogintoCrm(){
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("wiproasirin");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda353535**");
		WebElement login=driver.findElement(By.xpath("//input[@src='/TelsimGlobal/Menu/image3.gif']"));
		login.click();
	}
	@Test(priority=2)
	public void ClickActivation(){
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu").findElement(By.xpath("//a[@title='A K T � V A S Y O N']")).click();
	}
	
	@Test(priority=3)
	public void RoadToActivation() throws InterruptedException, ClassNotFoundException, SQLException{
		String gsm=null;
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='Submit']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='customerid']")).sendKeys(DBData(gsm));
		driver.findElement(By.xpath("//input[@value='customerno']")).click();
		driver.findElement(By.xpath("//input[@name='Submit']")).click();
		Thread.sleep(5000);
		String mainWindow = driver.getWindowHandle();
		System.out.println(mainWindow);
		Windows(driver, mainWindow);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(!driver.getTitle().toString().contains("secure")){
			driver.findElement(By.xpath("//input[@value='Tamam']")).click();
	}else
	{
		driver.findElement(By.xpath("//img[@class='actionIcon']")).click();
		driver.findElement(By.xpath("//a[@id='overridelink']")).click();
		driver.findElement(By.xpath("//input[@value='Tamam']")).click();
		}
		driver.switchTo().window(mainWindow);
	}
	public void Windows (WebDriver driver, String mainWindow) {
		
		mainWindow = driver.getWindowHandle();
		Set<String> totalwindowsIds = driver.getWindowHandles();
		int a=0;
		String[] myset = totalwindowsIds.toArray(new String[totalwindowsIds.size()]);	
			if(myset[a].contains(mainWindow)){
				driver.switchTo().window(myset[a+1]);
			}
			else{
				driver.switchTo().window(myset[a]);
			}
	}
	@Test(priority=4)
	public void RoadtoActivation2 (){
		
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@value='Devam']")).click();
		driver.switchTo().alert().accept();
	
	}
	@Test(priority=5)
	public void RoadtoActivation3() throws InterruptedException{
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	driver.switchTo().defaultContent();
	driver.switchTo().frame("cx");
	WebElement aboneislemleri=driver.findElement(By.xpath("//td[@class='passive' and @id='2' and @style='CURSOR: hand']"));
	aboneislemleri.click();
	}
	@Test(priority=6)
	public void SimCardSecimi() throws InterruptedException{
		String mainWindow = driver.getWindowHandle();
		driver.switchTo().defaultContent();
		switchwindow(driver,"cx","frame1","frame1");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='brand']")));
		selectObject.selectByValue("MC");
		Thread.sleep(2000);
		WebDriverWait wait1 = new WebDriverWait(driver, 15);
		wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[@name='prefix']")));
		Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@name='prefix']")));
		selectObject2.selectByValue("546");
		wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//select[@name='rsvstatus']")));
		Select selectObject3 = new Select (driver.findElement(By.xpath("//select[@name='rsvstatus']")));
		selectObject3.selectByValue("E");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@name='buttongsmno']")).sendKeys(Keys.ENTER);
		Thread.sleep(7000);
		Windows(driver,mainWindow);
		driver.findElement(By.xpath("//input[@name='selgsmno']")).sendKeys("702");
		driver.findElement(By.xpath("//input[@name='find']")).click();
		List<WebElement> gsms = driver.findElements(By.xpath("//input[@type='button']"));
		int i=0;
		gsms.get(i).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
		driver.findElement(By.xpath("//input[@type='button']")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.switchTo().window(mainWindow);
		
		switchwindow(driver,"cx","frame1","frame1");
		driver.findElement(By.xpath("//input[@name='simcardICCI']")).click();
		Windows(driver,mainWindow);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@name='find' and @type='submit']")).click();
		driver.findElement(By.xpath("//input[@type='button']")).click();
		driver.findElement(By.xpath("//input[@class='button']")).click();
		driver.switchTo().window(mainWindow);
		switchwindow(driver,"cx","frame1","frame1");  
		driver.findElement(By.xpath("//input[@name='handset']")).click(); //Unexpected error
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Windows(driver,mainWindow);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@name='handsetbrand']")).sendKeys("Alcatel HB 100");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@name='Bul']")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.cssSelector("//a[href*='AL00004']")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.switchTo().window(mainWindow);
		switchwindow(driver,"cx","frame1","frame1");
		driver.findElement(By.xpath("//input[@name='imeino']")).sendKeys("123456789123456");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("oto");
		switchwindow2(driver,"cx","frame1");
		WebElement paketsecimi=driver.findElement(By.xpath("//td[@class='passive' and @id='2' and @style='CURSOR: hand']"));
		paketsecimi.click();
	}
	
	public void switchwindow (WebDriver driver, String cx, String frame1, String frame2){
		//WebDriver x;
		driver.switchTo().frame("cx");
		driver.switchTo().frame("frame1");
		driver.switchTo().frame("frame1");	
	}
	public void switchwindow2 (WebDriver driver,String cx, String frame1){
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.switchTo().frame("frame1");
	}
	@Test(priority=7)
	public void PaketSecimi() throws InterruptedException{
		
		String mainWindow = driver.getWindowHandle();
		driver.switchTo().defaultContent();
		switchwindow(driver,"cx","frame1","frame1");
		driver.findElement(By.xpath("//input[@name='simcardICCI']")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Windows(driver,mainWindow);
		driver.findElement(By.cssSelector("//a[href*='11900']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.switchTo().window(mainWindow);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		switchwindow(driver,"cx","frame1","frame1");
		driver.findElement(By.xpath("//input[@name='handset']")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Windows(driver,mainWindow);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.cssSelector("//a[href*='PPHCN01']")).click(); //Unexpected Error
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.switchTo().window(mainWindow);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Windows(driver,mainWindow);
		driver.findElement(By.xpath("//input[@name='close']")).click();	
		driver.switchTo().window(mainWindow);
		switchwindow2(driver,"cx","frame1");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement odemetipi=driver.findElement(By.xpath("//td[@class='passive' and @id='3' and @style='CURSOR: hand']"));
		odemetipi.click();
	}
	@Test(priority=8)
	public void OdemeTipi (){
		
		driver.switchTo().defaultContent();
		switchwindow(driver,"cx","frame1","frame1");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='paytype']")));
		selectObject.selectByValue("Diger");
		driver.findElement(By.xpath("//input[@name='�deme �eklini se�']")).click();
		driver.findElement(By.xpath("//input[@name='pi']")).click();
		driver.findElement(By.xpath("//input[@name='Se�']")).click();
		switchwindow2(driver,"cx","frame1");
		WebElement faturadonemi=driver.findElement(By.xpath("//td[@class='passive' and @id='4' and @style='CURSOR: hand']"));
		faturadonemi.click();
	}
	@Test(priority=9)
	public void FaturaDonemi () throws InterruptedException{
		
		String mainWindow4 = driver.getWindowHandle();
		driver.switchTo().defaultContent();
		switchwindow(driver,"cx","frame1","frame1");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='billingperiod']")));
		selectObject.selectByValue("11");
		Thread.sleep(5000);
		driver.switchTo().alert().accept();
		driver.switchTo().window(mainWindow4);
		switchwindow2(driver,"cx","frame1");
		WebElement kampanyagiris=driver.findElement(By.xpath("//td[@class='passive' and @id='9' and @style='CURSOR: hand']"));
		kampanyagiris.click();		
	}
	@Test(priority=10)
	public void KampanyaGirisi (){
		
		switchwindow2(driver,"cx","frame1");
		WebElement kullanicigiris=driver.findElement(By.xpath("//td[@class='passive' and @id='8' and @style='CURSOR: hand']"));
		kullanicigiris.click();	
		
	}
	@Test(priority=11)
	public void KullaniciGirisi (){
		
		driver.switchTo().defaultContent();
		switchwindow(driver,"cx","frame1","frame1");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='reserveid']")));
		selectObject.selectByValue("K");
		switchwindow2(driver,"cx","frame1");
		WebElement vekilvasi=driver.findElement(By.xpath("//td[@class='passive' and @id='7' and @style='CURSOR: hand']"));
		vekilvasi.click();	
	}
	
	@Test(priority=12)
	public void VekilVasi(){
		
		switchwindow2(driver,"cx","frame1");
		WebElement ozellikgiris=driver.findElement(By.xpath("//td[@class='passive' and @id='6' and @style='CURSOR: hand']"));
		ozellikgiris.click();	
	}
	@Test(priority=13)
	public void OzellikGiris(){
		
		switchwindow2(driver,"cx","frame1");
		WebElement servisgiris=driver.findElement(By.xpath("//td[@class='passive' and @id='5' and @style='CURSOR: hand']"));
		servisgiris.click();	
	}
	@Test(priority=14)
	public void ServisGiris() throws InterruptedException{
		driver.switchTo().defaultContent();
		switchwindow(driver,"cx","frame1","frame1");
		driver.findElement(By.xpath("//input[@name='check0']")).click();
		driver.findElement(By.xpath("//input[@name='kaydet3']")).click();
		driver.findElement(By.xpath("//input[@type='submit']")).click();
	}
	@Test(priority=15)
	public void MakingActivation(){
		switchwindow2(driver,"cx","frame1");
		driver.findElement(By.xpath("//input[@id='activation']")).click();
		driver.switchTo().alert().accept();
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		System.out.println(driver.findElement(By.tagName("h2")).getText());
	
	}
	public static String DBData (String gsm) throws ClassNotFoundException, SQLException{
		
		  Class.forName("oracle.jdbc.OracleDriver");
		  Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
		  Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		  stmt.executeQuery("SELECT * FROM ccb.ccb_subscriber cs, ccb.ccb_subscriber_package csp, ccb.ccb_package cp, ccb.ccb_imsi_pool cip, ccb.ccb_gsm_pool cgp, ccb.ccb_icci_sim_pool cisp WHERE cs.status = 'A' AND cs.billing_period = '11' AND cs.gsm_no = csp.gsm_no AND cs.start_date = csp.start_date AND cs.gsm_no = cip.gsm_no AND cs.start_date = cip.start_date AND cip.icci = cisp.icci AND cs.customer_id IN (SELECT customer_id FROM ccb.ccb_customer WHERE     TYPE = 'S' AND NVL (blacklist_ind, 'H') = 'H' AND NVL (graylist_ind, 'H') = 'H' AND verified = 'Y' AND citizenship = 'TR') AND csp.package_id = cp.package_id AND cp.status = 'A' AND cp.post_ind = 'H' AND cp.brand_code = 'MC' AND cip.gsm_no = cgp.gsm_no AND cip.imsi_Status_code = 'AK' AND cip.brand_code = 'MC' AND cip.post_ind = 'H' AND NVL (cgp.number_type, 'X') <> 'S' --AND cgp.reservation_type = 'A' AND cgp.stock_type = 'N' AND cgp.STATUS = 'A' AND cisp.card_type = 'NO' AND cisp.usage_reason_code = 'AK' AND cisp.icci_status_code = 'AB' AND cisp.campaign_code = 'DEF_MC' ORDER BY cs.start_Date DESC;");
		   ResultSet rs = stmt.getResultSet();
		   int randomNum = 1 + (int)(Math.random() * 200);
		   System.out.println(randomNum);
		   rs.absolute(randomNum);
		   String s  = rs.getString("CUSTOMER_ID");
		   System.out.println(s);
			  gsm = s;
		return gsm;
	}
	@AfterClass
	public void quitdriver(){
		driver.quit();
	}

}
