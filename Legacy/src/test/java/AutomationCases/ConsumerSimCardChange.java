package AutomationCases;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;


public class ConsumerSimCardChange {

	
	WebDriver driver;
	
	//@SuppressWarnings("deprecation")
	@BeforeTest
	public void SetUp(){
		
		WebDriverManager.iedriver().setup();
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capabilities.setCapability("requireWindowFocus", true);
		capabilities.setCapability(CapabilityType.BROWSER_NAME,"internet explorer");
		capabilities.setCapability(CapabilityType.BROWSER_VERSION,"11");
		capabilities.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "normal");
		//File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		//System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		//DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
	    //cap.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "none");
		driver = new InternetExplorerDriver(capabilities);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");	
	}
	
	@Test(priority=1, description="Login functionality")
	public void LogintoCrm(){
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("fkaymaz");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda123456789**");
		WebElement login=driver.findElement(By.xpath("//input[@src='/TelsimGlobal/Menu/image3.gif']"));
		login.click();
	}
	
	@Test(priority=2, description="road to sim card change")
	public void SimCardV2 () throws InterruptedException, AWTException, ClassNotFoundException, SQLException{
	String gsm=null;
	driver.switchTo().frame("ax");
	driver.switchTo().frame("menu");
	driver.findElement(By.xpath("//a[@title='M��teri-Abone Listesi']")).click();
	driver.switchTo().defaultContent();
	driver.switchTo().frame("cx");
	driver.findElement(By.xpath("//input[@name='gsmno']")).sendKeys(DBConnection3(gsm));
	driver.findElement(By.xpath("//input[@name='bul']")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	driver.findElement(By.xpath("//input[@value='Sair i�lem']")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	driver.switchTo().defaultContent();
	driver.switchTo().frame("cx");
	Thread.sleep(1000);
	driver.findElement(By.cssSelector("//a[href*='301']")).click(); 
	Thread.sleep(1000);
	Robot robot = new Robot();
	robot.keyPress(KeyEvent.VK_ENTER);
	robot.keyRelease(KeyEvent.VK_ENTER);
	//driver.switchTo().alert().accept();
	}
	
	@Test(priority=3, description="Make Sim Card Change")
	
		public void SimCard() throws AWTException, InterruptedException, ClassNotFoundException, SQLException{		
			driver.switchTo().defaultContent();
			driver.switchTo().frame("cx");
			String mainWindoww = driver.getWindowHandle();
			System.out.println(mainWindoww);
			driver.findElement(By.xpath("//input[@name='simcardICCI']")).sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			Windows(driver, mainWindoww);
			String simcardicci = null;
			driver.findElement(By.xpath("//input[@name='icci']")).clear();
			driver.findElement(By.xpath("//input[@name='icci']")).sendKeys(DBConnection2(simcardicci));
			driver.findElement(By.xpath("//input[@value='BUL']")).click();
			//Thread.sleep(1000);
			driver.findElement(By.xpath("//input[@type='button']")).click();
			/*((JavascriptExecutor) driver).executeScript("window.showModalDialog = window.open;");
			WebElement element = driver.findElement(By.cssSelector("//input[class='button']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);*/
			//Actions act = new Actions(driver);
			//act.moveToElement(driver.findElement(By.cssSelector("//input[class='button']"))).clickAndHold().release().perform();
			//Point coordinates = driver.findElement(By.xpath("//input[@type='button']")).getLocation();
			//int xcord = coordinates.getX();
			//int ycord = coordinates.getY();
			Dimension dim = driver.findElement(By.cssSelector("//input[class='button']")).getSize();
			System.out.println(dim.height);
			System.out.println(dim.width);
			Robot robot = new Robot();
			robot.mouseMove(385,120);
			// ilki geni�lik
			robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
			robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);	
			//driver.findElement(By.cssSelector("//input[class='button']")).sendKeys(Keys.ENTER);
			//driver.navigate().refresh();
			//driver.manage().window().notify();
			//driver.manage().window().maximize();
			//driver.manage().deleteAllCookies();
			//driver.findElement(By.cssSelector("//input[class='button']")).click();
			//driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			System.out.println("sdsadasd");
			System.out.println(mainWindoww);
			Thread.sleep(2000);
			driver.switchTo().window(mainWindoww);
			System.out.println("sdsadasd");
		    driver.switchTo().defaultContent();
		    driver.switchTo().frame("cx");
		    driver.findElement(By.xpath("//input[@name='degistir']")).click();
			
		   
		}
		public void Windows (WebDriver driver, String mainWindow) {
			
			mainWindow = driver.getWindowHandle();
			Set<String> totalwindowsIds = driver.getWindowHandles();
			int a=0;
			String[] myset = totalwindowsIds.toArray(new String[totalwindowsIds.size()]);	
				if(myset[a].contains(mainWindow)){
					driver.switchTo().window(myset[a+1]);
				}
				else{
					driver.switchTo().window(myset[a]);
				}
		}
		public static String DBConnection2(String icci) throws ClassNotFoundException, SQLException{
			
			 Class.forName("oracle.jdbc.OracleDriver");
			   Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
			   Statement stmt = conn.createStatement();
			   stmt.executeQuery("select * from ccb.ccb_icci_sim_pool WHERE USAGE_REASON_CODE = 'YD' AND icci IN (SELECT icci FROM ccb.ccb_imsi_pool WHERE gsm_no IS NULL and brand_code='MC' and imsi_status_code='PS') AND CARD_TYPE = 'NO' AND ICCI_STATUS_CODE = 'BA' AND LOCK_IND = 'H' AND CAMPAIGN_CODE='DEF_MC'");
			   ResultSet rs = stmt.getResultSet();
			   if(rs.next()){
				  String s  = rs.getString(1);
				  icci = s;
			   }
			return icci;
		}
		public static String DBConnection3(String gsm) throws ClassNotFoundException, SQLException{
			   Class.forName("oracle.jdbc.OracleDriver");
			   Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
			   Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			   //stmt.executeQuery("SELECT * FROM ccb.ccb_subscriber cs, ccb.ccb_subscriber_package csp, ccb.ccb_package cp, ccb.ccb_imsi_pool cip, ccb.ccb_gsm_pool cgp, ccb.ccb_icci_sim_pool cisp WHERE cs.status = 'A' AND cs.billing_period = '11' AND cs.gsm_no = csp.gsm_no AND cs.start_date = csp.start_date AND cs.gsm_no = cip.gsm_no AND cs.start_date = cip.start_date AND cip.icci = cisp.icci AND cs.customer_id IN (SELECT customer_id FROM ccb.ccb_customer WHERE     TYPE = 'S' AND NVL (blacklist_ind, 'H') = 'H' AND NVL (graylist_ind, 'H') = 'H' AND verified = 'Y' AND citizenship = 'TR') AND csp.package_id = cp.package_id AND cp.status = 'A' AND cp.post_ind = 'H' AND cp.brand_code = 'MC' AND cip.gsm_no = cgp.gsm_no AND cip.imsi_Status_code = 'AK' AND cip.brand_code = 'MC' AND cip.post_ind = 'H' AND NVL (cgp.number_type, 'X') <> 'S' --AND cgp.reservation_type = 'A' AND cgp.stock_type = 'N' AND cgp.STATUS = 'A' AND cisp.card_type = 'NO' AND cisp.usage_reason_code = 'AK' AND cisp.icci_status_code = 'AB' AND cisp.campaign_code = 'DEF_MC' ORDER BY cs.start_Date DESC;");
			   stmt.executeQuery("SELECT * FROM ccb.ccb_subscriber cs, ccb.ccb_subscriber_package csp, ccb.ccb_package cp, ccb.ccb_imsi_pool cip, ccb.ccb_gsm_pool cgp, ccb.ccb_icci_sim_pool cisp WHERE     cs.status = 'A' AND cs.billing_period = '11' AND cs.gsm_no = csp.gsm_no AND cs.start_date = csp.start_date AND cs.gsm_no = cip.gsm_no AND cs.start_date = cip.start_date AND cip.icci = cisp.icci AND cs.customer_id IN (SELECT customer_id FROM ccb.ccb_customer WHERE     TYPE = 'S' AND NVL (blacklist_ind, 'H') = 'H' AND NVL (graylist_ind, 'H') = 'H' AND verified = 'Y' AND citizenship = 'TR') AND csp.package_id = cp.package_id AND cp.status = 'A' AND cp.post_ind = 'H' AND cp.brand_code = 'MC' AND cip.gsm_no = cgp.gsm_no AND cip.imsi_Status_code = 'AK' AND cip.brand_code = 'MC' AND cip.post_ind = 'H' AND NVL (cgp.number_type, 'X') <> 'S' --AND cgp.reservation_type = 'A' AND cgp.stock_type = 'N' AND cgp.STATUS = 'A' AND cisp.card_type = 'NO' AND cisp.usage_reason_code = 'AK' AND cisp.icci_status_code = 'AB' AND cisp.campaign_code = 'DEF_MC' ORDER BY cs.start_Date DESC;");
			   ResultSet rs = stmt.getResultSet();
			   int randomNum = 1 + (int)(Math.random() * 200);
			   System.out.println(randomNum);
			   rs.absolute(randomNum);
			   String s  = rs.getString("GSM_NO");
			   System.out.println(s);
				  gsm = s;
			   return gsm;
		}
	
}
