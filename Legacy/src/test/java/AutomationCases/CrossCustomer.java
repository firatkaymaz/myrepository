package AutomationCases;
import org.openqa.selenium.WebDriver;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import oracle.net.aso.d;


public class CrossCustomer {
	
	private static final WebDriver WebDriverRefrence = null;
	WebDriver driver;
	
	@BeforeTest
	public void SetUp(){
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		//DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
	    //capabilities.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "none");
		driver = new InternetExplorerDriver();
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");	
	}
	
	@Test(priority=1, description="Login functionality")
	public void LogintoCrm(){
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("fkaymaz");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda123456789**");
		WebElement login=driver.findElement(By.xpath("//input[@src='/TelsimGlobal/Menu/image3.gif']"));
		login.click();
	}
	@Test(priority=2)
	public void ClickActivation(){
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='M��teri-Abone Listesi']")).click();
	}
	
	@Test(priority=3)
	public void Inquiry() throws ClassNotFoundException, SQLException{
		String gsm=null;
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='gsmno']")).sendKeys(DBData(gsm));
		driver.findElement(By.xpath("//input[@name='bul']")).sendKeys(Keys.ENTER);	
	}
	@Test(priority=4)
	public void RoadToCross() throws InterruptedException{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");	
		driver.findElement(By.xpath("//input[@value='Sair i�lem']")).sendKeys(Keys.ENTER);	
		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.cssSelector("//a[href*='400']")).sendKeys(Keys.ENTER);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
	}
	@Test(priority=5)
	public void CrossTransfer() throws InterruptedException{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");	
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='transaction']")));
		selectObject.selectByValue("ABONE_DEVRI_NORMAL");
		Thread.sleep(2000);
		Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@name='treason']")));
		selectObject2.selectByValue("DUZELTME1");
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
		//WebDriverWait wait = new WebDriverWait(WebDriverRefrence,20);
		//WebElement description;
		//WebElement description= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("input//[@name='description']")));
		//description.findElement(By.xpath("input//[@name='description']")).sendKeys("devir");
		driver.findElement(By.cssSelector("input[name='description']")).sendKeys("devir");
		//driver.findElement(By.xpath("input//[@name='description']")).sendKeys("devir");
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		driver.findElement(By.cssSelector("input[name='kaydet']")).sendKeys(Keys.ENTER);
		//Thread.sleep(3000);
	}
	@Test(priority=6)
	public void RoadtoCrossTransfer() throws ClassNotFoundException, SQLException{
		String customerno=null;
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");	
		driver.findElement(By.cssSelector("input[name='Submit']")).sendKeys(Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(200,TimeUnit.SECONDS) ;
		driver.findElement(By.xpath("//input[@name='customerid']")).sendKeys(DBData2(customerno));
		
	}
	/*@AfterTest
	public void Ending(){
		driver.close();
	}*/
	
	public static String DBData (String gsm) throws ClassNotFoundException, SQLException{
		
		  Class.forName("oracle.jdbc.OracleDriver");
		  Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
		  Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		  stmt.executeQuery("SELECT * FROM ccb.ccb_subscriber cs, ccb.ccb_subscriber_package csp, ccb.ccb_package cp, ccb.ccb_imsi_pool cip, ccb.ccb_gsm_pool cgp, ccb.ccb_icci_sim_pool cisp WHERE cs.status = 'A' AND cs.billing_period = '11' AND cs.gsm_no = csp.gsm_no AND cs.start_date = csp.start_date AND cs.gsm_no = cip.gsm_no AND cs.start_date = cip.start_date AND cip.icci = cisp.icci AND cs.customer_id IN (SELECT customer_id FROM ccb.ccb_customer WHERE     TYPE = 'S' AND NVL (blacklist_ind, 'H') = 'H' AND NVL (graylist_ind, 'H') = 'H' AND verified = 'Y' AND citizenship = 'TR') AND csp.package_id = cp.package_id AND cp.status = 'A' AND cp.post_ind = 'H' AND cp.brand_code = 'MC' AND cip.gsm_no = cgp.gsm_no AND cip.imsi_Status_code = 'AK' AND cip.brand_code = 'MC' AND cip.post_ind = 'H' AND NVL (cgp.number_type, 'X') <> 'S' --AND cgp.reservation_type = 'A' AND cgp.stock_type = 'N' AND cgp.STATUS = 'A' AND cisp.card_type = 'NO' AND cisp.usage_reason_code = 'AK' AND cisp.icci_status_code = 'AB' AND cisp.campaign_code = 'DEF_MC' ORDER BY cs.start_Date DESC;");
		   ResultSet rs = stmt.getResultSet();
		   int randomNum = 1 + (int)(Math.random() * 200);
		   System.out.println(randomNum);
		   rs.absolute(randomNum);
		   String s  = rs.getString("GSM_NO");
		   System.out.println(s);
			  gsm = s;
		return gsm;
	}
	public static String DBData2 (String gsm) throws ClassNotFoundException, SQLException{
		
		  Class.forName("oracle.jdbc.OracleDriver");
		  Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
		  Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		  stmt.executeQuery("SELECT * FROM ccb.ccb_subscriber cs, ccb.ccb_subscriber_package csp, ccb.ccb_package cp, ccb.ccb_imsi_pool cip, ccb.ccb_gsm_pool cgp, ccb.ccb_icci_sim_pool cisp WHERE cs.status = 'A' AND cs.billing_period = '11' AND cs.gsm_no = csp.gsm_no AND cs.start_date = csp.start_date AND cs.gsm_no = cip.gsm_no AND cs.start_date = cip.start_date AND cip.icci = cisp.icci AND cs.customer_id IN (SELECT customer_id FROM ccb.ccb_customer WHERE     TYPE = 'S' AND NVL (blacklist_ind, 'H') = 'H' AND NVL (graylist_ind, 'H') = 'H' AND verified = 'Y' AND citizenship = 'TR') AND csp.package_id = cp.package_id AND cp.status = 'A' AND cp.post_ind = 'H' AND cp.brand_code = 'MC' AND cip.gsm_no = cgp.gsm_no AND cip.imsi_Status_code = 'AK' AND cip.brand_code = 'MC' AND cip.post_ind = 'H' AND NVL (cgp.number_type, 'X') <> 'S' --AND cgp.reservation_type = 'A' AND cgp.stock_type = 'N' AND cgp.STATUS = 'A' AND cisp.card_type = 'NO' AND cisp.usage_reason_code = 'AK' AND cisp.icci_status_code = 'AB' AND cisp.campaign_code = 'DEF_MC' ORDER BY cs.start_Date DESC;");
		   ResultSet rs = stmt.getResultSet();
		   int randomNum = 1 + (int)(Math.random() * 200);
		   System.out.println(randomNum);
		   rs.absolute(randomNum);
		   String s  = rs.getString("CUSTOMER_ID");
		   System.out.println(s);
			  gsm = s;
		return gsm;
	}

}
