package Stepdefinitions;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MernisSorgulama {

	WebDriver driver;
	
	@Given("^access to LegacyCRM$")
	public void AccessLCRM (){
		
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		driver = new InternetExplorerDriver();
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");
	}
	@And("^login to LegacyCRM$")
	public void Login(){
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("wiprobaslan");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda123456789**");
		WebElement login=driver.findElement(By.xpath("//input[@src='/TelsimGlobal/Menu/image3.gif']"));
		login.click();
	}
	@And("^click Yeni Sorgulama in CCB menu$")
	public void YeniSorgulama(){
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='M��teri-Abone Listesi']")).click();
	}
	@And("^Type gsm number and list the customer$")
	public void InquiryCustomer(){
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='gsmno']")).sendKeys("5467028662");
		driver.findElement(By.xpath("//input[@name='bul']")).sendKeys(Keys.ENTER);
	}
	
	@And("^Click Sair Islemler button$")
	public void SairIslem() throws InterruptedException{
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@value='Sair i�lem']")).sendKeys(Keys.ENTER);
	}
	
	@And("^Click Mernis Kimlik Dogrulama link$")
	public void MernisLink() throws InterruptedException{
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.cssSelector("//a[href*='396']")).sendKeys(Keys.ENTER);
	}
	
	@When("^Pressing TC Kimlik No Sorgulama button$")
	public void TcKimlikNo(){
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='treason']")));
		selectObject.selectByValue("AB.ISTEGI");
		driver.findElement(By.xpath("//input[@name='description']")).sendKeys("mernis");
		driver.findElement(By.xpath("//input[@name='T.C. Kimlik No Sorgulama']")).sendKeys(Keys.ENTER);
	}
	
	@Then("^See that tc id is verified through Mernis$")
	public void VerificationComment() throws InterruptedException{
	Thread.sleep(2000);
	String a = driver.findElement(By.xpath("//b[contains(text(),'Islem Basarili Olmustur.')]")).getText();
	System.out.println(a);
	}
	
}
